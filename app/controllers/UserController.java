package controllers;

import com.google.inject.Inject;
import javassist.NotFoundException;
import play.Logger;
import play.libs.Json;
import play.mvc.Result;
import services.DataCollector;

import javax.naming.ServiceUnavailableException;
import java.util.concurrent.CompletionStage;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class UserController extends play.mvc.Controller {
    @Inject
    DataCollector dataCollector;
    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public CompletionStage<Result> users() {
        Logger.info("Incoming request to get user list ");
        return  dataCollector.getUsers().thenApply(users-> ok(Json.toJson(users)))
                .exceptionally(e-> {
                    if(e.getCause() instanceof ServiceUnavailableException){
                        Logger.error("error while access to BitBucket: " + e.getCause().getMessage());
                        return status(SERVICE_UNAVAILABLE,"Service unavailable");//USER C
                    }
                    Logger.error("error : " + e.getMessage());
                    return  internalServerError();
                });
    }

    public CompletionStage<Result> lastCommits(String user) {
        Logger.info("Incoming request to get user last commits list for : " + user);
        return  dataCollector.getLastCommits(user).thenApply(commits-> {
            Logger.info("Found " + commits.size()+ " commits for : " + user);
            return ok(Json.toJson(commits));
        }).exceptionally(e-> {
            if(e.getCause() instanceof NotFoundException){
                Logger.info("user not found : "+ user );
                return notFound("User not found");
            }
            else if(e.getCause() instanceof ServiceUnavailableException){
                Logger.error("error while access to BitBucket: " + e.getCause().getMessage());
                return status(SERVICE_UNAVAILABLE,"Service unavailable");//USER C
            }
            Logger.error("error: " + e.getCause().getMessage());
            return  internalServerError("Internal server error");
        });
    }

}

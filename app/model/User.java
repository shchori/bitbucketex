package model;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.sql.Timestamp;
import java.time.OffsetDateTime;

public class User {

    private String displayName;
    private String userName;
    private String website;
    private String id;
    private Timestamp createdOn;

    public User(){}

    public User(String displayName, String userName, String website, String id, Timestamp createdOn) {
        this.displayName = displayName;
        this.userName = userName;
        this.website = website;
        this.id = id;
        this.createdOn = createdOn;
    }


    public String getDisplayName() {
        return displayName;
    }

    @JsonSetter("display_name")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getUserName() {
        return userName;
    }

    @JsonSetter("username")
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getWebsite() {
        return website;
    }

    @JsonSetter("website")
    public void setWebsite(String website) {
        this.website = website;
    }

    public String getId() {
        return id;
    }

    @JsonSetter("account_id")
    public void setId(String id) {
        this.id = id;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    @JsonSetter("created_on")
    public void setCreatedOn(String createdOn) {
        this.createdOn =  Timestamp.from( OffsetDateTime.parse( createdOn ).toInstant() );
    }
}
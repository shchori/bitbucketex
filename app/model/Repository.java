package model;

import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.JsonNode;

import java.sql.Timestamp;
import java.time.OffsetDateTime;

public class Repository {
    private String scm;
    private String description;
    private String name;
    private String commitUrl;
    private Timestamp updateOn;

    public Repository(){}

    public Repository(String scm, String description, String name, String commitUrl,Timestamp updateOn) {
        this.scm = scm;
        this.description = description;
        this.name = name;
        this.commitUrl = commitUrl;
        this.updateOn = updateOn;
    }


    public String getScm() {
        return scm;
    }

    @JsonSetter("scm")
    public void setScm(String scm){ this.scm = scm; }

    public String getDescription() {
        return description;
    }

    @JsonSetter("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    @JsonSetter("name")
    public void setName(String name) {
        this.name = name;

    }public String getCommitUrl() {
        return commitUrl;
    }

    @JsonSetter("links")
    public void setCommitUrl(JsonNode links) {
        this.commitUrl = links.get("commits").get("href").asText().replace("!","");
    }


    public Timestamp getUpdateOn() {
        return updateOn;
    }

    public String getUpdateOnString() {
        return updateOn.toString();
    }

    @JsonSetter("updated_on")
    public void setUpadeOn(String time) {
        this.updateOn =  Timestamp.from( OffsetDateTime.parse( time ).toInstant() );
    }


}

package model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.JsonNode;

import java.sql.Timestamp;
import java.time.OffsetDateTime;

public class Commit{
    private Timestamp time;
    private String hash;
    private String repoName;
    private String message;
    private String userName ;

    public Commit(Timestamp time, String hash, String repoName, String message, String userName) {
        this.time = time;
        this.hash = hash;
        this.repoName = repoName;
        this.message = message;
        this.userName = userName;
    }

    public Commit(){};
    @JsonIgnore
    public Timestamp getTimeAtTimestamp() {
        return time;
    }

    @JsonGetter("date")
    public String getTime() {
        return time.toString();
    }

    @JsonSetter("date")
    public void setTime(String time) {
        this.time =  Timestamp.from( OffsetDateTime.parse( time ).toInstant() );
    }

    @JsonGetter("hash")
    public String getHash() {
        return hash;
    }

    @JsonSetter("hash")
    public void setHash(String hash) {
        this.hash = hash;
    }

    @JsonGetter("repository-name")
    public String getRepoName() {
        return repoName;
    }

    @JsonSetter("repository")
    public void setRepoName(JsonNode repository) {
        this.repoName = repository.get("name").asText();
    }

    @JsonSetter("message")
    public String getMessage() {
        return message;
    }

    @JsonGetter("message")
    public void setMessage(String message) {
        this.message = message;
    }
    @JsonIgnore
    public String getUserName() {
        return userName;
    }

    @JsonSetter("author")
    public void setUserName(JsonNode author) {
        JsonNode user = author.get("user");
        if(user == null){
            this.userName = "unknown";
            return;
        }
        this.userName = user.get("username").asText();
    }

}

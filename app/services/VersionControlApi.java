package services;

import com.google.inject.ImplementedBy;
import model.*;

import java.util.List;
import java.util.concurrent.CompletionStage;

@ImplementedBy(BitBucketApi.class)
interface VersionControlApi {
    public CompletionStage<List<User>> getUsers(String project);
    public CompletionStage<List<Repository>> getLastUpdateRepositories(String project);
    public CompletionStage<List<Commit>> getLastCommits(Repository repo);

}

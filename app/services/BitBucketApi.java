package services;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import com.typesafe.config.Config;
import model.Commit;
import model.Repository;
import model.User;
import play.libs.Json;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

public class BitBucketApi implements VersionControlApi {
    private String baseUrl ;
    private  ApiPagesCollector apiPagesCollector;

    @Inject
    BitBucketApi(Config config, ApiPagesCollector apiPagesCollector){
        String user = config.getString("bitbucket.user"),
            password = config.getString("bitbucket.password");
        this.apiPagesCollector = apiPagesCollector;
        apiPagesCollector.setAuth(user,password);
        this.baseUrl = config.getString("bitbucket.url");
    }

    @Override
    public CompletionStage<List<User>> getUsers(String project) {
        String url = String.format("%s/teams/%s/members", baseUrl, project);
        return this.apiPagesCollector.getAllPagesValues(url)
                .thenApply(pagesValue -> this.getClassListFromPagesValue(pagesValue,User[].class));
    }



    @Override
    public CompletionStage<List<Repository>> getLastUpdateRepositories(String project) {
        String url = String.format("%s/repositories/%s/?sort=-updated_on",baseUrl,project) ;
        return this.apiPagesCollector.getAllLastUpdatePages(url)
                .thenApply(pagesValue -> this.<Repository>getClassListFromPagesValue(pagesValue,Repository[].class))
                .thenApply(repositories->{
                    repositories.removeIf(repository -> !this.inTheLast24Hours(repository.getUpdateOn()));
                    return repositories;
                });
    }

    @Override
    public CompletionStage<List<Commit>> getLastCommits(Repository repo) {
        String url = repo.getCommitUrl();
        return this.apiPagesCollector.getAllLastUpdatePages(url)
                .thenApply(pagesValue -> this.<Commit>getClassListFromPagesValue(pagesValue,Commit[].class))
                .thenApply(commits->{
                    commits.removeIf(commit -> !this.inTheLast24Hours(commit.getTimeAtTimestamp()));
                    return commits;
                });
    }

    private <T> List<T> getClassListFromPagesValue(List<JsonNode> valuesNodeArray, Class t){
        return  valuesNodeArray.stream().map(jsonNode -> {
                T[]  values= (T[])Json.fromJson(jsonNode,t);
                return Arrays.asList(values);
            }).flatMap(List::stream).collect(Collectors.toList());
         }

    private Boolean inTheLast24Hours(Timestamp time){
        return  !time.before(Timestamp.from(Instant.now().minus(24, ChronoUnit.HOURS)));
    }
}

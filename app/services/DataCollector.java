package  services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.typesafe.config.Config;
import javassist.NotFoundException;
import model.Commit;
import model.User;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

@Singleton
public class DataCollector{
    VersionControlApi sourceControlManager;
    private String project ;

    @Inject
    DataCollector(Config config,  VersionControlApi versionControlApi){
        this.sourceControlManager = versionControlApi;
        this.project = config.getString("bitbucket.project");
    }

    public CompletionStage<List<String>> getUsers(){
        return sourceControlManager.getUsers(this.project)
                .thenApply(this::getUsersNameList);
    }

    public CompletionStage<List<Commit>> getLastCommits(String userName) {
        return sourceControlManager.getUsers(this.project).thenCompose(users->{
            if(!isUserExist(users,userName)){
                throw new CompletionException(new NotFoundException("User does not exist"));
            }
            return sourceControlManager.getLastUpdateRepositories(this.project).thenCompose(repos -> {
                List<CompletableFuture> futuresList = repos.stream().map(repository ->
                     sourceControlManager.getLastCommits(repository)
                            .thenApply(commits->this.filterUserNameCommits(commits,userName))
                            .toCompletableFuture()
                ).collect(Collectors.toList());
                CompletableFuture<Void> allDoneFuture =
                        CompletableFuture.allOf(futuresList.toArray(new CompletableFuture[futuresList.size()]));
                return allDoneFuture.thenApply(v ->
                        futuresList.stream()
                                .map(future -> (List<Commit>) future.join())
                                .flatMap(List::stream).collect(Collectors.toList()));
            });
        });
    }

    private Boolean isUserExist(List<User> users,String username){
        for(User user : users){
            if (user.getUserName().equals(username)){
                return  true;
            }
        }
        return false;
    }


    private List<String> getUsersNameList(List<User> users){
            return  users.stream().map(User::getUserName).collect(Collectors.toList());

    }

    private List<Commit> filterUserNameCommits(List<Commit> commits,String username){
        commits.removeIf(commit -> !commit.getUserName().equals(username));
        return commits;
    }

}

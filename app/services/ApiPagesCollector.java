package services;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;
import play.libs.ws.WSAuthScheme;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;

import javax.naming.ServiceUnavailableException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

@Singleton
public class ApiPagesCollector {

    private ScheduledExecutorService threadPool;
    private WSClient ws;
    private RetryPolicy retryPolicy;
    private String user;
    private String password;


    @Inject
    ApiPagesCollector(WSClient ws) {
        this.ws = ws;
        threadPool = Executors.newScheduledThreadPool(5);
        this.retryPolicy = new RetryPolicy().retryOn(TooManyRequestException.class).withBackoff(100, 500000, TimeUnit.MILLISECONDS);

    }

    public void setAuth(String user, String password) {
        this.user = user;
        this.password = password;
    }


    public CompletionStage<List<JsonNode>> getAllPagesValues(String url) {
        return getPagesJsonValueUntil(url,(jsonNode)->false);
    }

    public CompletionStage<List<JsonNode>> getAllLastUpdatePages(String url) {
        return getPagesJsonValueUntil(url,(jsonNode)->{
            List<Timestamp> dates = getUpdateDatesArrayFromJson(jsonNode);
            for(Timestamp date : dates){
                if(!inTheLast24Hours(date)) {
                    return true;
                }
            }
            return false;
        });
    }

    private CompletionStage<WSResponse> getWithRetry(String url) {
        return Failsafe.with(this.retryPolicy).with(threadPool)
                .future(() -> ws.url(url).setAuth(this.user, this.password, WSAuthScheme.BASIC).get().thenApply(response -> {
                    if (response.getStatus() == 429) {
                        throw new CompletionException(new TooManyRequestException());
                    } else if (response.getStatus() != 200) {
                        throw new CompletionException(new ServiceUnavailableException("Error while access to api : " + response.getBody()));
                    }
                    return response;
                }));
    }

    private Boolean isTheLastPage(WSResponse response){
        return  response.asJson().get("next")== null;
    }

    private CompletionStage<List<JsonNode>> getPagesJsonValueUntil(String url,PagesHandler pagesHandler) {
        return this.getWithRetry(url).thenCompose(response -> {
            JsonNode responseValues = response.asJson().get("values");
            List<JsonNode> valuesArray = new ArrayList<JsonNode>();
            valuesArray.add(responseValues);
            if (isTheLastPage(response) || pagesHandler.stopLoadPage(responseValues )) {
                return CompletableFuture.completedFuture(valuesArray);
            }
            String nextUrl = response.asJson().get("next").asText().replace("!", "");
            return getPagesJsonValueUntil(nextUrl,pagesHandler).thenApply(list -> {
                valuesArray.addAll(list);
                return valuesArray;
            });
        });
    }


    private Boolean inTheLast24Hours(Timestamp time){
        return  !time.before(Timestamp.from(Instant.now().minus(24, ChronoUnit.HOURS)));
    }

    List<Timestamp> getUpdateDatesArrayFromJson(JsonNode jsonNode){
        List<String> datesString  = null;
        if(jsonNode.get(0).get("date")!=null){
            datesString = jsonNode.findValuesAsText("date");
        }
        if(jsonNode.get(0).get("updated_on")!=null){
            datesString = jsonNode.findValuesAsText("updated_on");
        }
        return datesString.stream().map(s->Timestamp.from( OffsetDateTime.parse( s ).toInstant())).collect(Collectors.toList());
    }

    private class TooManyRequestException extends Throwable {
        TooManyRequestException() { };
    }

    private interface  PagesHandler{
            public Boolean stopLoadPage(JsonNode values);
    }
}
package services;

import com.typesafe.config.Config;
import javassist.NotFoundException;
import model.Commit;
import model.Repository;
import model.User;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

import static org.hamcrest.CoreMatchers.isA;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class DataCollectorTest {

    @Mock
    private Config config;

    @Mock
    private BitBucketApi bitBucketApi;

    private DataCollector dataCollector;


    @Before
    public void setUp(){
        //set configuration
        Mockito.when(config.getString("bitbucket.project")).thenReturn("innovid");
        this.dataCollector = new DataCollector(config, bitBucketApi);
    }
    @Test
    public void testGetUser_returnAllUsers(){
        User user1 = new User("user1","user1","user1","user1", Timestamp.from(Instant.now()));
        User user2 = new User("user2","user2","user2","user2", Timestamp.from(Instant.now()));
        List<User> usersList = Arrays.asList(user1,user2);
        Mockito.when(bitBucketApi.getUsers("innovid")).thenReturn(CompletableFuture.completedFuture(usersList));
        List<String> usersResult = dataCollector.getUsers().toCompletableFuture().join();
        String[] usersTest = {"user1","user2"};
        assertArrayEquals(usersResult.toArray(),usersTest);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testGetLastCommits_UnKnownUserExp(){
        List<User> usersList = new ArrayList<User>();
        Mockito.when(bitBucketApi.getUsers("innovid")).thenReturn(CompletableFuture.completedFuture(usersList));
        thrown.expect(CompletionException.class);
        thrown.expectCause(isA(NotFoundException.class));
        thrown.expectMessage("User does not exist");
        dataCollector.getLastCommits("not exist").toCompletableFuture().join();
    }

    @Test
    public void testGetLastCommits_returnAllUserLastCommit(){
        //init bitbucket data
        User user1 = new User("user1","user1","user1","user1", Timestamp.from(Instant.now()));
        User user2 = new User("user2","user2","user2","user2", Timestamp.from(Instant.now()));
        Commit commit1 = new Commit(Timestamp.from(Instant.now()),"commit1","dev",
                "commit1","user1");
        Commit commit2 = new Commit(Timestamp.from(Instant.now().minus(6, ChronoUnit.HOURS)),"commit2","dev",
                "commit2","user2");
        Commit commit3 = new Commit(Timestamp.from(Instant.now().minus(12, ChronoUnit.HOURS)),"commit3","sisera",
                "commit3","user1");
        Commit commit4 = new Commit(Timestamp.from(Instant.now().minus(18, ChronoUnit.HOURS)),"commit4","sisera",
                "commit4","user2");
        Repository repo1 = new Repository("git","","dev","",null);
        Repository repo2 = new Repository("git","","sisera","",null);
        List<User> usersList = Arrays.asList(user1,user2);
        List<Repository> repositories = Arrays.asList(repo1,repo2);
        List<Commit> commitsRepo1 = Arrays.asList(commit1,commit2);
        List<Commit> commitsRepo2 = Arrays.asList(commit3,commit4);
        Mockito.when(bitBucketApi.getUsers("innovid")).thenReturn(CompletableFuture.completedFuture(new ArrayList<User>(usersList)));
        Mockito.when(bitBucketApi.getLastUpdateRepositories("innovid")).thenReturn(CompletableFuture.completedFuture(new ArrayList<Repository>(repositories)));
        Mockito.when(bitBucketApi.getLastCommits(repo1)).thenReturn(CompletableFuture.completedFuture(new ArrayList<Commit>(commitsRepo1)));
        Mockito.when(bitBucketApi.getLastCommits(repo2)).thenReturn(CompletableFuture.completedFuture(new ArrayList<Commit>(commitsRepo2)));
        List<Commit> lastCommits = dataCollector.getLastCommits("user1").toCompletableFuture().join();
        assertTrue(lastCommits.contains(commit1));
        assertTrue(lastCommits.contains(commit3));
        assertEquals(lastCommits.size(),2);
    }
}

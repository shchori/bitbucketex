package services;

import com.fasterxml.jackson.databind.JsonNode;
import com.typesafe.config.Config;
import model.Commit;
import model.Repository;
import model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import play.libs.Json;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BitBucketApiTest {
    @Mock
    private ApiPagesCollector apiPagesCollector;
    @Mock
    private Config config;

    private BitBucketApi bitBucketApi;

    @Before
    public void setUp() {
        //set configuration
        when(config.getString("bitbucket.user")).thenReturn("user");
        when(config.getString("bitbucket.password")).thenReturn("password");
        when(config.getString("bitbucket.url")).thenReturn("url");
        bitBucketApi = new BitBucketApi(this.config, apiPagesCollector);
    }

    @Test
    public void getUsers_returnUserList(){
        JsonNode userPage1 = getFileAsJson("./test/resources/user-page1.json").get("values");
        JsonNode userPage2 = getFileAsJson("./test/resources/user-page2.json").get("values");
        List<JsonNode> pages = Arrays.asList(userPage1,userPage2);
        when(apiPagesCollector.getAllPagesValues("url/teams/innovid/members"))
                .thenReturn(CompletableFuture.completedFuture(pages));
        List<User> users = bitBucketApi.getUsers("innovid").toCompletableFuture().join();
        assertEquals(65, users.size());
    }

    @Test
    public void getLastCommit_returnOnlyLastDayCommits(){
        JsonNode commitPage1 = getFileAsJson("./test/resources/commit-page1.json").get("values");
        JsonNode commitPage2 = getFileAsJson("./test/resources/commit-page2.json").get("values");
        List<JsonNode> pages = Arrays.asList(commitPage1,commitPage2);
        Repository repo = new Repository("git","repo","repo","url/repo/commits",Timestamp.from(Instant.now()));
        when(apiPagesCollector.getAllLastUpdatePages("url/repo/commits"))
                .thenReturn(CompletableFuture.completedFuture(pages));
        List<Commit> commits = bitBucketApi.getLastCommits(repo).toCompletableFuture().join();
        assertEquals(6, commits.size());
    }

    @Test
    public void getLastRepository_returnOnlyLastDaUpdateRepositories(){
        JsonNode repoPage1 = getFileAsJson("./test/resources/repository-page1.json").get("values");
        JsonNode repoPage2 = getFileAsJson("./test/resources/repository-page2.json").get("values");
        List<JsonNode> pages = Arrays.asList(repoPage1,repoPage2);
        when(apiPagesCollector.getAllLastUpdatePages("url/repositories/innovid/?sort=-updated_on"))
                .thenReturn(CompletableFuture.completedFuture(pages));
        List<Repository> repositories = bitBucketApi.getLastUpdateRepositories("innovid").toCompletableFuture().join();
        assertEquals(7, repositories.size());

    }
//
//    @Test
//    public void testGetCommits_OnePage() {
//        setUrlResponse("url/repositories/innovid/dev/commits", 200, "./test/resources/commit-page3.json");
//        Repository repository = new Repository("git", "test", "dev", "url/repositories/innovid/dev/commits");
//        List<Commit> commits = bb.getCommits("innovid", repository).toCompletableFuture().join();
//        assertEquals(30, commits.size());
//    }
//
//    @Test
//    public void testGetCommits_CheckPagingOnly2Pages() {
//        setUrlResponse("url/repositories/innovid/dev/commits", 200, "./test/resources/commit-page1.json");
//        setUrlResponse("url/repositories/innovid/dev/commits?page=2", 200, "./test/resources/commit-page2.json");
//        Repository repository = new Repository("git", "test", "dev", "url/repositories/innovid/dev/commits");
//        List<Commit> commits = bb.getCommits("innovid", repository).toCompletableFuture().join();
//        assertEquals("",60, commits.size());
//    }
//
//    @Test
//    public void testGetCommits_TooManyRequestBackoff() {
//        setUrlResponseAfter429("url/repositories/innovid/dev/commits", 200, "./test/resources/commit-page1.json");
//        setUrlResponseAfter429("url/repositories/innovid/dev/commits?page=2", 200, "./test/resources/commit-page2.json");
//        Repository repository = new Repository("git", "test", "dev", "url/repositories/innovid/dev/commits");
//        List<Commit> commits = bb.getCommits("innovid", repository).toCompletableFuture().join();
//        assertEquals(60, commits.size());
//    }
//
//    @Test
//    public void testGetCommits_Error() {
//        setUrlErrorResponse("url/repositories/innovid/dev/commits", 500, "Error while loading data");
//        Repository repository = new Repository("git", "test", "dev", "url/repositories/innovid/dev/commits");
//        List<Commit> commits = bb.getCommits("innovid", repository).toCompletableFuture().exceptionally(e -> {
//            assertNotNull(e);
//            assertTrue(e.getMessage().contains("BitBucketError : \nError while loading data"));
//            return new ArrayList<Commit>();
//        }).join();
//    }
//
//    @Test
//    public void testGetRepositorys_OnePage() {
//        setUrlResponse("url/repositories/innovid", 200, "./test/resources/repository-page3.json");
//        List<Repository> repositories = bb.getRepsitories("innovid").toCompletableFuture().join();
//        assertEquals(10, repositories.size());
//
//    }
//
//    @Test
//    public void testGetRepositorys_Paging() {
//        setUrlResponse("url/repositories/innovid", 200, "./test/resources/repository-page1.json");
//        setUrlResponse("url/repositories/innovid?page=2", 200, "./test/resources/repository-page2.json");
//        setUrlResponse("url/repositories/innovid?page=3", 200, "./test/resources/repository-page3.json");
//        setUrlResponseAfter429("url/repositories/innovid?page=3", 200, "./test/resources/repository-page3.json");
//        List<Repository> repositories = bb.getRepsitories("innovid").toCompletableFuture().join();
//        assertEquals(30, repositories.size());
//    }
//
//    @Test
//    public void testGetRepositorys_TooManyRequestBackoff() {
//        setUrlResponseAfter429("url/repositories/innovid", 200, "./test/resources/repository-page1.json");
//        setUrlResponseAfter429("url/repositories/innovid?page=2", 200, "./test/resources/repository-page2.json");
//        setUrlResponseAfter429("url/repositories/innovid?page=3", 200, "./test/resources/repository-page3.json");
//        List<Repository> repositories = bb.getRepsitories("innovid").toCompletableFuture().join();
//        assertEquals(30, repositories.size());
//    }
//
//    @Test
//    public void testGetRepositorys_Error() {
//        setUrlErrorResponse("url/repositories/innovid", 500, "Error while loading data");
//        List<Repository> repositories = bb.getRepsitories("innovid").toCompletableFuture().exceptionally(e -> {
//            assertNotNull(e);
//            assertTrue(e.getMessage().contains("BitBucketError : \nError while loading data"));
//            return new ArrayList<Repository>();
//        }).join();
//    }
//
//    @Test
//    public void testGetUsers_OnePage() {
//        setUrlResponse("url/teams/innovid/members", 200, "./test/resources/user-page2.json");
//        List<User> users = bb.getUsers("innovid").toCompletableFuture().join();
//        assertEquals(15, users.size());
//
//    }
//
//    @Test
//    public void testGetUsers_Paging() {
//        setUrlResponse("url/teams/innovid/members", 200, "./test/resources/user-page1.json");
//        setUrlResponse("url/teams/innovid/members?page=2", 200, "./test/resources/user-page2.json");
//        List<User> users = bb.getUsers("innovid").toCompletableFuture().join();
//        assertEquals(65, users.size());
//    }
//
//    @Test
//    public void testGetUsers_TooManyRequestBackoff() {
//        setUrlResponseAfter429("url/teams/innovid/members", 200, "./test/resources/user-page1.json");
//        setUrlResponseAfter429("url/teams/innovid/members?page=2", 200, "./test/resources/user-page2.json");
//        List<User> users = bb.getUsers("innovid").toCompletableFuture().join();
//        assertEquals(65, users.size());
//
//    }
//
//    @Test
//    public void testGetUsers_Error() {
//        setUrlErrorResponse("url/teams/innovid/members", 500, "Error while loading data");
//        List<User> users = bb.getUsers("innovid").toCompletableFuture().exceptionally(e -> {
//            assertNotNull(e);
//            assertTrue(e.getMessage().contains("BitBucketError : \nError while loading data"));
//            return new ArrayList<User>();
//        }).join();
//    }
//
//    private void setUrlResponse(String url, int status, String jsonPath) {
//        WSRequest wsRequest = mock(WSRequest.class);
//        WSResponse wsResponse = mock(WSResponse.class);
//        JsonNode jsonRes = getFileAsJson(jsonPath);
//        Mockito.when(ws.url(url)).thenReturn(wsRequest);
//        Mockito.when(wsRequest.setAuth("user", "password", WSAuthScheme.BASIC)).thenReturn(wsRequest);
//        Mockito.when(wsRequest.get()).thenReturn(CompletableFuture.completedFuture(wsResponse));
//        Mockito.when(wsResponse.asJson()).thenReturn(jsonRes);
//        Mockito.when(wsResponse.getStatus()).thenReturn(status);
//    }
//
//    private void setUrlErrorResponse(String url, int status, String message) {
//        WSRequest wsRequest = mock(WSRequest.class);
//        WSResponse wsResponse = mock(WSResponse.class);
//        Mockito.when(ws.url(url)).thenReturn(wsRequest);
//        Mockito.when(wsRequest.setAuth("user", "password", WSAuthScheme.BASIC)).thenReturn(wsRequest);
//        Mockito.when(wsRequest.get()).thenReturn(CompletableFuture.completedFuture(wsResponse));
//        Mockito.when(wsResponse.getBody()).thenReturn(message);
//        Mockito.when(wsResponse.getStatus()).thenReturn(status);
//    }
//
//    private void setUrlResponseAfter429(String url, int status, String jsonPath) {
//        WSRequest wsRequest = mock(WSRequest.class);
//        WSResponse wsResponse = mock(WSResponse.class);
//        JsonNode jsonRes = getFileAsJson(jsonPath);
//        Mockito.when(ws.url(url)).thenReturn(wsRequest);
//        Mockito.when(wsRequest.setAuth("user", "password", WSAuthScheme.BASIC)).thenReturn(wsRequest);
//        Mockito.when(wsRequest.get()).thenReturn(CompletableFuture.completedFuture(wsResponse));
//        Mockito.when(wsResponse.getStatus()).thenReturn(429).thenReturn(status);
//        Mockito.when(wsResponse.asJson()).thenReturn(jsonRes);
//    }

    private static JsonNode getFileAsJson(String filePath) {
        String content = "{}";

        try {
            content = new String(Files.readAllBytes(Paths.get(filePath)));
            content = content.replace("*currentTime*", Instant.now().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }


        return Json.parse(content);
    }
}

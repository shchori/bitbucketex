package services;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import play.libs.Json;
import play.libs.ws.WSAuthScheme;
import play.libs.ws.WSClient;
import play.libs.ws.WSRequest;
import play.libs.ws.WSResponse;

import javax.naming.ServiceUnavailableException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

import static org.hamcrest.CoreMatchers.isA;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class ApiPageCollectorTest {
    @Mock
    private WSClient ws;

    ApiPagesCollector apiPagesCollector;


    @Before
    public void setUp() {
        this.apiPagesCollector = new ApiPagesCollector(ws);
        this.apiPagesCollector.setAuth("user","password");
    }
    @Test
    public void getAllPagesValues_returnAllPages() {
        setUrlResponse("url/teams/innovid/members", 200, "./test/resources/user-page1.json");
        setUrlResponse("url/teams/innovid/members?page=2", 200, "./test/resources/user-page2.json");
        List<JsonNode> pages = apiPagesCollector.getAllPagesValues("url/teams/innovid/members").toCompletableFuture().join();
        assertEquals(2,pages.size());
    }
    @Test
    public void getAllPagesValues_returnAllPagesAfterRetry() {
        setUrlResponseAfter429("url/teams/innovid/members", 200, "./test/resources/user-page1.json");
        setUrlResponseAfter429("url/teams/innovid/members?page=2", 200, "./test/resources/user-page2.json");
        List<JsonNode> pages = apiPagesCollector.getAllPagesValues("url/teams/innovid/members").toCompletableFuture().join();
        assertEquals(2,pages.size());
    }

    @Test
    public void getAllLastUpdatePages_CommitSyle_returnAllLastUpdatePages() {
        setUrlResponse("url/repositories/innovid/dev/commits", 200, "./test/resources/commit-page1.json");
        setUrlResponse("url/repositories/innovid/dev/commits?page=2", 200, "./test/resources/commit-page2.json");
        List<JsonNode> pages = apiPagesCollector.getAllLastUpdatePages("url/repositories/innovid/dev/commits").toCompletableFuture().join();
        assertEquals(2,pages.size());
    }
    @Test
    public void getAllLastUpdatePages_RepoSyle_returnAllLastUpdatePages() {
        setUrlResponse("url/repositories/innovid", 200, "./test/resources/repository-page1.json");
        setUrlResponse("url/repositories/innovid?page=2", 200, "./test/resources/repository-page2.json");
        List<JsonNode> pages = apiPagesCollector.getAllLastUpdatePages("url/repositories/innovid").toCompletableFuture().join();
        assertEquals(2,pages.size());
    }
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void getAllPagesValues_throwWhenCantAccessBitBuckets() {
        setUrlErrorResponse("url/teams/innovid/members", 500, "");
        thrown.expect(CompletionException.class);
        thrown.expectCause(isA(ServiceUnavailableException.class));
        thrown.expectMessage("Error while access to api : ");
        List<JsonNode> pages = apiPagesCollector.getAllPagesValues("url/teams/innovid/members").toCompletableFuture().join();
    }

    private void setUrlResponse(String url, int status, String jsonPath) {
        WSRequest wsRequest = mock(WSRequest.class);
        WSResponse wsResponse = mock(WSResponse.class);
        JsonNode jsonRes = getFileAsJson(jsonPath);
        Mockito.when(ws.url(url)).thenReturn(wsRequest);
        Mockito.when(wsRequest.setAuth("user", "password", WSAuthScheme.BASIC)).thenReturn(wsRequest);
        Mockito.when(wsRequest.get()).thenReturn(CompletableFuture.completedFuture(wsResponse));
        Mockito.when(wsResponse.asJson()).thenReturn(jsonRes);
        Mockito.when(wsResponse.getStatus()).thenReturn(status);
    }

    private void setUrlErrorResponse(String url, int status, String message) {
        WSRequest wsRequest = mock(WSRequest.class);
        WSResponse wsResponse = mock(WSResponse.class);
        Mockito.when(ws.url(url)).thenReturn(wsRequest);
        Mockito.when(wsRequest.setAuth("user", "password", WSAuthScheme.BASIC)).thenReturn(wsRequest);
        Mockito.when(wsRequest.get()).thenReturn(CompletableFuture.completedFuture(wsResponse));
        Mockito.when(wsResponse.getBody()).thenReturn(message);
        Mockito.when(wsResponse.getStatus()).thenReturn(status);
    }

    private void setUrlResponseAfter429(String url, int status, String jsonPath) {
        WSRequest wsRequest = mock(WSRequest.class);
        WSResponse wsResponse = mock(WSResponse.class);
        JsonNode jsonRes = getFileAsJson(jsonPath);
        Mockito.when(ws.url(url)).thenReturn(wsRequest);
        Mockito.when(wsRequest.setAuth("user", "password", WSAuthScheme.BASIC)).thenReturn(wsRequest);
        Mockito.when(wsRequest.get()).thenReturn(CompletableFuture.completedFuture(wsResponse));
        Mockito.when(wsResponse.getStatus()).thenReturn(429).thenReturn(status);
        Mockito.when(wsResponse.asJson()).thenReturn(jsonRes);
    }

    private static JsonNode getFileAsJson(String filePath) {
        String content = "{}";

        try {
            content = new String(Files.readAllBytes(Paths.get(filePath)));
            content = content.replace("*currentTime*", Instant.now().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }


        return Json.parse(content);
    }

}

package controllers;

import org.junit.Test;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.mvc.Http;
import play.mvc.Result;
import play.test.WithApplication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static play.test.Helpers.*;

public class UserControllerTest extends WithApplication {

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder().build();
    }

    @Test
    public void testUsers() {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/users");

        Result result = route(app, request);
        assertEquals("application/json", result.contentType().get());
        assertEquals(200, result.status());
        assertTrue(contentAsString(result).contains("shchori"));

    }

    @Test
    public void testLastCommits() {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/users/shchori/commits");

        Result result = route(app, request);
        assertEquals(200, result.status());
        assertEquals("application/json", result.contentType().get());

    }

    @Test
    public void testLastCommitsUnknownUser() {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/shchori2/commits");

        Result result = route(app, request);
        assertEquals(404, result.status());

    }
}

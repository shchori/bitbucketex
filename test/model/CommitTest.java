package model;

import org.junit.Test;
import play.libs.Json;

import java.sql.Timestamp;
import java.time.OffsetDateTime;

import static org.junit.Assert.assertEquals;

public class CommitTest {
    @Test
    public void commitFromJson(){
        String commitJson = "{\"hash\":\"c9c9affa15d2317837e682c74b7b24df8f04fb15\",\"repository\":{\"name\":\"dev\"},\"author\":{\"user\":{\"username\":\"tanyaz\"}},\"date\":\"2018-12-23T14:44:35+00:00\",\"message\":\"KAN-17535 ( create BDD tests ) [ added dcmintegration to test bundle integrations controller ]\\n\"}";
        Commit commit = Json.fromJson(Json.parse(commitJson),Commit.class);
        assertEquals(commit.getUserName(),"tanyaz");
        assertEquals(commit.getMessage(),"KAN-17535 ( create BDD tests ) [ added dcmintegration to test bundle integrations controller ]\n");
        assertEquals(commit.getTime(),"2018-12-23 16:44:35.0");
        assertEquals(commit.getTimeAtTimestamp(), Timestamp.from( OffsetDateTime.parse( "2018-12-23T14:44:35+00:00" ).toInstant()));
        assertEquals(commit.getRepoName(), "dev");
        assertEquals(commit.getHash(),"c9c9affa15d2317837e682c74b7b24df8f04fb15");
    }

    @Test
    public void commitToJson(){
        Commit commit = new Commit(Timestamp.from( OffsetDateTime.parse( "2018-12-23T14:44:35+00:00" ).toInstant()),"123","repository","message","user");
        String commitStringJson = Json.toJson(commit).toString();
        assertEquals(commitStringJson,"{\"message\":\"message\",\"date\":\"2018-12-23 16:44:35.0\",\"hash\":\"123\",\"repository-name\":\"repository\"}");
        return;
    }

    @Test
    public void testSetCommitNullUserName() {
        Commit commit = new Commit();
        commit.setUserName(Json.parse("{}"));
        assertEquals(commit.getUserName(),"unknown");

    }

}

package model;

import org.junit.Test;
import play.libs.Json;

import static org.junit.Assert.assertEquals;

public class RepositoryTest {
    @Test
    public void userFromJson(){
        String repositoryString = "{\"scm\":\"git\",\"links\":{\"commits\":{\"href\":\"https://bitbucket.org/!api/2.0/repositories/innovid/dev/commits\"}},\"description\":\"description\",\"name\":\"dev\"}";
        Repository repo = Json.fromJson(Json.parse(repositoryString),Repository.class);
        assertEquals(repo.getScm(),"git");
        assertEquals(repo.getCommitUrl(),"https://bitbucket.org/api/2.0/repositories/innovid/dev/commits");
        assertEquals(repo.getName(),"dev");
        assertEquals(repo.getDescription(), "description");

    }

}

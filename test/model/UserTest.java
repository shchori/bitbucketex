package model;

import org.junit.Test;
import play.libs.Json;

import java.sql.Timestamp;
import java.time.OffsetDateTime;

import static org.junit.Assert.assertEquals;

public class UserTest {
    @Test
    public void userFromJson(){
        String userString = "{\"website\":\"www.example.com\",\"uuid\":\"128568ca-4045-40e6-bd11-de6a011198b4\",\"created_on\":\"2012-12-23T15:46:52.317875+00:00\",\"username\":\"advir1\",\"display_name\":\"Amit Dvir\",\"account_id\":\"557058:814aaf3c-05d2-410c-92ad-483d8f3b3e50\"}";
        User user = Json.fromJson(Json.parse(userString),User.class);
        assertEquals(user.getUserName(),"advir1");
        assertEquals(user.getWebsite(),"www.example.com");
        assertEquals(user.getId(),"557058:814aaf3c-05d2-410c-92ad-483d8f3b3e50");
        assertEquals(user.getCreatedOn(), Timestamp.from( OffsetDateTime.parse( "2012-12-23T15:46:52.317875+00:00" ).toInstant()));
        assertEquals(user.getDisplayName(),"Amit Dvir");
    }

}

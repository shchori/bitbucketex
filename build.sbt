name := """BitbucketAPi"""
organization := "com.innovid"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.7"

libraryDependencies += guice

libraryDependencies += "org.mockito" % "mockito-core" % "2.10.0" % "test"
libraryDependencies += "net.jodah" % "failsafe" % "1.1.1"

libraryDependencies ++= Seq(
  ws,
  javaWs
) 